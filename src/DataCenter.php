<?php

namespace rhtds\utils;

use chick1993\util\Curl;
use chick1993\util\libs\exceptions\RuntimeException;

class DataCenter
{
    /**
     * @var string
     */
    private $appId;

    /**
     * @var string
     */
    private $appToken;

    /**
     * @var string
     */
    private $gateWay;

    protected function __construct(string $appId, string $appToken, string $gateWay = '')
    {
        $this->appId = $appId;
        $this->appToken = $appToken;
        $this->gateWay = $gateWay;
    }

    static public function init(string $appId, string $appToken, string $gateWay = ''): self
    {
        return new self($appId, $appToken, $gateWay);
    }

    /**
     * 发起GET请求
     * @param $api
     * @param array $params
     * @return array
     */
    public function get($api, array $params = []): array
    {
        $url = $this->getUrl($api);
        $params = array_merge($params, self::getSignData($params));
        $url = $url . '?' . http_build_query($params);
        return Curl::get($url)->toArray();
    }

    /**
     * 发起POST请求
     * @param $api
     * @param array $data
     * @param bool $biz 是否包装数据到biz对象
     * @return array
     */
    public function post($api, array $data = [], bool $biz = true): array
    {
        $signData = self::getSignData($data);
        if ($biz) {
            $signData['biz'] = $data;
        } else {
            $signData = array_merge($data, $signData);
        }
        $url = self::getUrl($api);
        return Curl::postJson($url, $signData)->toArray();
    }

    /**
     * 是否请求测试环境
     * @param bool $sandbox
     * @return $this
     */
    public function sandbox(bool $sandbox = true): self
    {
        if ($sandbox) {
            $this->gateWay = 'http://192.168.0.5/';
        }
        return $this;
    }

    /**
     * 获取签名数据
     * @param array $data
     * @return array
     */
    protected function getSignData(array $data): array
    {
        $appId = $this->appId;
        $timestamp = time();
        $signData['app_id'] = $appId;
        $signData['sign_stamp'] = $timestamp;
        $signData['sign'] = $this->getSignV1($timestamp);
        $signData['version'] = 'v1';
        return $signData;
    }

    public function getSignV1(int $timestamp): string
    {
        return strtoupper(md5($this->appToken . $timestamp));
    }

    public function getSignV2(array $data): string
    {
        $signData = $data;
        $signData['token'] = $this->appToken;
        ksort($signData, SORT_NATURAL);
        $str = http_build_query($signData);
        return md5($str);
    }

    /**
     * 获取请求URL
     * @param $api
     * @return string
     */
    protected function getUrl($api): string
    {
        $apiGetWay = $this->gateWay;
        if (empty($apiGetWay)) throw new RuntimeException('gateWay not be empty');
        return "$apiGetWay/$api";
    }

}